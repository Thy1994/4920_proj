package com.example.zak.auction_backend.Objects;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

import java.util.ArrayList;
import java.util.List;

@Entity
public class Transaction {
    @Id public String id;
    @Index public long product;
    @Index public String seller;
    @Index public String buyer;

    public Transaction(){
    }

    public Transaction(long product, String seller, String buyer){
        this.product = product;
        this.seller = seller;
        this.buyer = buyer;
    }
}
