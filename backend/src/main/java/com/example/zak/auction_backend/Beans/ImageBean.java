package com.example.zak.auction_backend.Beans;

public class ImageBean {
    String image;

    public ImageBean(){
        this.image = "-";
    }

    public ImageBean(String image){
        this.image = image;
    }

    public void setImage(String image){
        this.image = image;
    }

    public String getImage(){
        return this.image;
    }
}
