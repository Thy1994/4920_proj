package com.example.zak.auction_backend.Objects;

import com.example.zak.auction_backend.TransactionEngine;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.condition.IfTrue;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Locale;

@Entity
public class Product {
    @Id public Long id;

    @Index private String name;
    @Index private Integer price;
    @Index private String seller;
    @Index private Date dateClosed;

    private String buyer;
    private Date datePosted;
    private String description;

    private String thumbnail;
    private List<String> pictures;

    @Index(IfTrue.class) public boolean active;
    private List<String> bidders;

    public Product() {}

    public Product(String seller, String name, String description, int price, int minutes)
    {
        this.seller = seller;
        this.bidders = new ArrayList<>();

        this.name = name;
        this.description = description;
        this.price = price;

        this.active = true;
        this.datePosted = new Date();
        this.dateClosed = new Date( ( new Date() ).getTime() + (minutes * 60 * 1000) );

        this.thumbnail = "";
        this.pictures = new ArrayList<>();
    }

    public String getName()
    {
        return this.name;
    }

    public String getDescription()
    {
        return this.description;
    }

    public int getPrice()
    {
        return this.price;
    }

    public long getId() { return this.id; }

    public String getSeller() { return this.seller; }

    public String getBuyer() { return this.buyer; }

    public List<String> getBidders() { return this.bidders; }

    public Date getDatePosted() {
        return this.datePosted;
    }

    public Date getDateClosed() {
        return this.dateClosed;
    }

    public void addThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public void addPictures(List<String> pictures){
        if( this.pictures == null )
        {
            this.pictures = new ArrayList<>();
        }
        this.pictures.addAll(pictures);
    }

    public void addPicture(String picture){
        if( this.pictures == null )
        {
            this.pictures = new ArrayList<>();
        }
        this.pictures.add(picture);
    }

    public boolean bid(String buyer, int price) {
        if(!checkActive()) return false;
        if (!buyer.equals(this.seller) && price > this.price) {
            this.price = price;
            this.buyer = buyer;
            if( this.bidders == null )
            {
                this.bidders = new ArrayList<>();
            }
            this.bidders.add(buyer);
            return true;
        }
        return false;
    }

    ProductInfo getProductInfo() {
        ProductInfo pi = new ProductInfo();
        pi.name = this.name;

        if( this.thumbnail == null )
        {
            this.thumbnail = "";
        }
        pi.thumbnail = this.thumbnail;

        pi.seller = this.seller;
        pi.price = this.price;
        pi.id = this.id;

        return pi;
    }

    public FullProductInfo getFullProductInfo() {
        FullProductInfo fpi = new FullProductInfo();
        fpi.name = this.name;
        if( this.pictures == null )
        {
            this.pictures = new ArrayList<>();
        }
        fpi.pictures = new ArrayList<>(this.pictures);
        fpi.description = this.description;
        fpi.seller = this.seller;
        fpi.price = this.price;
        fpi.id = this.id;
        fpi.datePosted = this.datePosted;
        fpi.dateClosed = this.dateClosed;

        return fpi;
    }

    public void print()
    {
        System.out.println(String.format(new Locale("en"), "%d: %s posted at %s for %d cents, auction ends %s. Description: %s", this.id, this.name, this.datePosted.toString(), this.price, this.dateClosed.toString(), this.description));
    }

    public static List<ProductInfo> getProductInfoList(Collection<Product> products) {
        List<ProductInfo> l = new ArrayList<>();
        for( Product p : products )
        {
            p.checkActive();
            l.add(p.getProductInfo());
        }
        return l;
    }

    public static List<ProductInfo> getProductInfoList(Collection<Product> products, boolean active) {
        List<ProductInfo> l = new ArrayList<>();
        for( Product p : products )
        {
            if( p.checkActive() == active)
            {
                l.add(p.getProductInfo());
            }
        }
        return l;
    }

    private boolean checkActive(){
        if (!active) return false;
        if( dateClosed.before(new Date()) ){
            TransactionEngine.completeTransaction(this);
        }
        return active;
    }

    public static void checkAllActive(List<Product> l){
        for(Product p : l) p.checkActive();
    }

    public class ProductInfo {
        public String name;
        public String seller;
        public int price;
        public String thumbnail;
        public long id;

        ProductInfo(){}
    }

    public class FullProductInfo {
        public String name;
        public String description;
        public List<String> pictures;
        public Date datePosted;
        public Date dateClosed;
        public String seller;
        public int price;
        public long id;

        FullProductInfo(){}
    }
}
