package com.example.zak.auction_backend.Beans;

import com.example.zak.auction_backend.Objects.Transaction;

import java.util.List;

/** The object model for the data we are sending through endpoints */
public class TransactionsBean {

    private List<Transaction> myData;

    public List<Transaction> getData() {
        return myData;
    }

    public void setData(List<Transaction> data) {
        myData = data;
    }
}