/*
   For step-by-step instructions on connecting your Android application to this backend module,
   see "App Engine Java Endpoints Module" template documentation at
   https://github.com/GoogleCloudPlatform/gradle-appengine-templates/tree/master/HelloEndpoints
*/

package com.example.zak.auction_backend;

import com.example.zak.auction_backend.Beans.FullProductInfoBean;
import com.example.zak.auction_backend.Beans.ImageBean;
import com.example.zak.auction_backend.Beans.KeyBean;
import com.example.zak.auction_backend.Beans.NotificationsBean;
import com.example.zak.auction_backend.Beans.ProductsInfoBean;
import com.example.zak.auction_backend.Beans.ResultBean;
import com.example.zak.auction_backend.Beans.TransactionsBean;
import com.example.zak.auction_backend.Objects.AuctionUser;
import com.example.zak.auction_backend.Objects.Notification;
import com.example.zak.auction_backend.Objects.Product;
import com.example.zak.auction_backend.Objects.Transaction;
import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiNamespace;
import com.google.appengine.repackaged.com.google.api.client.util.Base64;
import com.googlecode.objectify.NotFoundException;
import com.googlecode.objectify.ObjectifyService;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.security.GeneralSecurityException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.crypto.Cipher;
import javax.inject.Named;

/** An endpoint class we are exposing */
@Api(
  name = "auctionAppApi",
  version = "v1",
  namespace = @ApiNamespace(
    ownerDomain = "auction_backend.zak.example.com",
    ownerName = "auction_backend.zak.example.com",
    packagePath=""
  )
)
public class AuctionAppEndpoint {
    private final Map<String, PrivateKey> keys = new HashMap<>();

    static {
        if( ObjectifyService.ofy().load().type(Product.class).count() == 0 )
        {
            List<Product> startingProducts = new ArrayList<>();
            startingProducts.add( new Product("other_seller", "Samsung Note 7", "Good condition: minor explosive damage", 50000, 100000) );
            startingProducts.add( new Product("other_seller", "Horse Mask", "Brown horse mask, made from latex. Unused.", 1130, 100000) );
            startingProducts.add( new Product("other_seller", "Horse", "Brown horse", 100000, 100000) );
            startingProducts.add( new Product("other_seller", "Bugatti Veyron", "Moving up to a faster car after finishing learners driving test, minimal usage only", 165000000, 100000) );
            startingProducts.add( new Product("username", "Twigs", "A bundle of 5 twigs collected from around UNSW campus", 350, 100000) );
            startingProducts.add( new Product("username", "$200 Dick Smith Gift Voucher", "Selling $200 gift voucher at great price", 5000, 100000) );
            ObjectifyService.ofy().save().entities(startingProducts).now();
        }

        if( ObjectifyService.ofy().load().type(AuctionUser.class).count() == 0 )
        {
            try {
                String user = "username";
                String hashed = Base64.encodeBase64String( MessageDigest.getInstance("MD5").digest( "password".getBytes() ) );
                String email = "default@email.com";
                ObjectifyService.ofy().save().entity(new AuctionUser(user, hashed, email)).now();
            }
            catch( NoSuchAlgorithmException e )
            {
                e.printStackTrace();
            }
        }
    }

    @ApiMethod(name = "requestKey", httpMethod = "GET")
    public KeyBean requestKey(@Named("user") String user)
    {
        try {
            KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
            keyGen.initialize(1024);

            KeyPair pair = keyGen.generateKeyPair();

            keys.put(user, pair.getPrivate());

            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            new ObjectOutputStream(bos).writeObject( pair.getPublic() );
            byte[] keyBytes = bos.toByteArray();

            String keyString = Base64.encodeBase64String(keyBytes);

            KeyBean bean = new KeyBean();
            bean.setData( keyString );

            return bean;
        } catch (NoSuchAlgorithmException | IOException e)
        {
            e.printStackTrace();
        }
        return new KeyBean();
    }

    @ApiMethod(name = "isUser", httpMethod = "GET")
    public ResultBean isUser(@Named("user") String user)
    {
        ResultBean result = new ResultBean();
        AuctionUser u = ObjectifyService.ofy().load().type(AuctionUser.class).id(user).now();
        result.setSuccess(u != null);
        return result;
    }

    @ApiMethod(name = "getEmail", httpMethod = "GET")
    public ResultBean getEmail(@Named("user") String user)
    {
        ResultBean result = new ResultBean();
        AuctionUser u = ObjectifyService.ofy().load().type(AuctionUser.class).id(user).now();
        if(u != null){
            result.setSuccess(true);
            result.setMessage(u.getEmail());
        }
        return result;
    }

    @ApiMethod(name = "createUser", httpMethod = "POST")
    public ResultBean createUser(@Named("user") String user, @Named("pass") String encrypted,
                                 @Named("email") String email)
    {
        ResultBean result = new ResultBean();
        try {
            if( keys.containsKey(user) )
            {
                PrivateKey privatekey = keys.get(user);
                keys.remove(user);

                Cipher decrypt = Cipher.getInstance("RSA/ECB/PKCS1Padding");
                decrypt.init(Cipher.DECRYPT_MODE, privatekey);
                String hashed = Base64.encodeBase64String( decrypt.doFinal( Base64.decodeBase64(encrypted) ) );

                AuctionUser u = new AuctionUser(user, hashed, email);
                ObjectifyService.ofy().save().entity( u ).now();

                result.setSuccess(true);
                return result;
            } else
            {
                result.setMessage("Key not found.");
            }
        } catch (GeneralSecurityException e)
        {
            e.printStackTrace();
        }
        return result;
    }

    @ApiMethod(name = "loginUser", httpMethod = "GET")
    public ResultBean loginUser(@Named("user") String user, @Named("pass") String encrypted)
    {
        ResultBean result = new ResultBean();
        try{
            if( keys.containsKey(user) )
            {
                PrivateKey privatekey = keys.get(user);
                keys.remove(user);

                Cipher decrypt = Cipher.getInstance("RSA/ECB/PKCS1Padding");
                decrypt.init(Cipher.DECRYPT_MODE, privatekey);
                String hashed = Base64.encodeBase64String( decrypt.doFinal( Base64.decodeBase64(encrypted) ) );

                try {
                    AuctionUser u = ObjectifyService.ofy().load().type(AuctionUser.class).id(user).safe();

                    if( u.verify(hashed) ) {
                        result.setSuccess( true );
                    } else {
                        result.setMessage("Incorrect password.");
                    }
                } catch (NotFoundException e) {
                    result.setSuccess( false );
                    result.setMessage("User not found.");
                }
            } else
            {
                result.setMessage("Key not found.");
            }
        } catch (GeneralSecurityException e)
        {
            e.printStackTrace();
        }
        return result;
    }

    @ApiMethod(name = "changeEmail", httpMethod = "PUT")
    public ResultBean changeEmail(@Named("user") String user, @Named("email") String email)
    {
        ResultBean result = new ResultBean();
        AuctionUser u = ObjectifyService.ofy().load().type(AuctionUser.class).id(user).now();
        System.out.println("EMAIL ERRORS");


        if(u != null ) {
            u.changeEmail(email);
            ObjectifyService.ofy().save().entity(u).now();
            result.setSuccess(true);
        }
        return result;

    }


    @ApiMethod(name = "changePassword", httpMethod = "PUT")
    public ResultBean changePassword(@Named("user") String user, @Named("pass") String encrypted,
                                     @Named("new_pass") String new_encrypted)
    {
        ResultBean result = new ResultBean();
        try{
            if( keys.containsKey(user) )
            {
                PrivateKey privatekey = keys.get(user);
                keys.remove(user);

                Cipher decrypt = Cipher.getInstance("RSA/ECB/PKCS1Padding");
                decrypt.init(Cipher.DECRYPT_MODE, privatekey);
                String hashed = Base64.encodeBase64String( decrypt.doFinal( Base64.decodeBase64(encrypted) ) );
                String new_hashed = Base64.encodeBase64String( decrypt.doFinal( Base64.decodeBase64(new_encrypted) ) );

                try {
                    AuctionUser u = ObjectifyService.ofy().load().type(AuctionUser.class).id(user).safe();

                    if( u.verify(hashed) ) {
                        u.changePassword(new_hashed);
                        ObjectifyService.ofy().save().entity( u ).now();
                        result.setSuccess( true );
                    } else {
                        result.setMessage("Incorrect password.");
                    }
                } catch (NotFoundException e) {
                    result.setSuccess( false );
                    result.setMessage("User not found.");
                }
            } else
            {
                result.setMessage("Key not found.");
            }
        } catch (GeneralSecurityException e)
        {
            e.printStackTrace();
        }
        return result;
    }

    @ApiMethod(name = "getProducts", path="get_products", httpMethod = "GET")
    public ProductsInfoBean getProducts() {
        ProductsInfoBean response = new ProductsInfoBean();
        List<Product> products = ObjectifyService.ofy().load().type(Product.class).order("price").list();
        response.setData( Product.getProductInfoList( products, true ) );

        return response;
    }

    @ApiMethod(name = "getProductsByOthers", path="get_others_products", httpMethod = "GET")
    public ProductsInfoBean getProductsByOthers(@Named("user") String user) {
        ProductsInfoBean response = new ProductsInfoBean();
        List<Product> products = ObjectifyService.ofy().load().type(Product.class).filter("seller !=", user).list();
        response.setData( Product.getProductInfoList( products, true ) );

        return response;
    }

    @ApiMethod(name = "getProductsByUser", path="get_user_products", httpMethod = "GET")
    public ProductsInfoBean getProductsByUser(@Named("user") String user) {
        ProductsInfoBean response = new ProductsInfoBean();
        List<Product> products = ObjectifyService.ofy().load().type(Product.class).filter("seller ==", user).list();
        response.setData( Product.getProductInfoList( products, true ) );

        return response;
    }

    @ApiMethod(name = "getBids", path="get_bids", httpMethod = "GET")
    public ProductsInfoBean getBids(@Named("user") String user) {
        ProductsInfoBean response = new ProductsInfoBean();
        List<Product> products = new ArrayList<>();

        AuctionUser u = ObjectifyService.ofy().load().type(AuctionUser.class).id(user).now();
        for( Long id : u.getBids() )
        {
            products.add(ObjectifyService.ofy().load().type(Product.class).id(id).now());
        }
        response.setData( Product.getProductInfoList( products, true ) );
        return response;
    }

    @ApiMethod(name = "searchProducts", path="search_products", httpMethod = "GET")
    public ProductsInfoBean searchProducts(@Named("user") String user, @Named("search") String search) {
        ProductsInfoBean response = new ProductsInfoBean();
        List<Product> products = new ArrayList<>();

        List<String> words = new ArrayList<>(Arrays.asList(search.split(" ")));
        List<Product> allProducts = ObjectifyService.ofy().load().type(Product.class).filter("seller !=", user).list();
        for ( Product p : allProducts )
        {
            for ( String w : words )
            {
                if(p.getName().toLowerCase().contains(w.toLowerCase()))
                {
                    products.add(p);
                    break;
                }
            }
        }
        response.setData( Product.getProductInfoList( products, true ) );
        return response;
    }

    @ApiMethod(name = "getFullProduct", path="get_full_product", httpMethod = "GET")
    public FullProductInfoBean getFullProduct(@Named("id") long id) {
        FullProductInfoBean response = new FullProductInfoBean();
        Product p = ObjectifyService.ofy().load().type(Product.class).id(id).now();

        response.setData( p == null ? null : p.getFullProductInfo() );
        return response;
    }

    @ApiMethod(name = "deleteProduct", httpMethod = "DELETE")
    public ResultBean deleteProduct(@Named("id") long id) {
        ResultBean result = new ResultBean();
        Product p = ObjectifyService.ofy().load().type(Product.class).id(id).now();

        boolean success = false;
        if( p != null )
        {
            ObjectifyService.ofy().delete().entity(p).now();
            success = true;
        }
        result.setSuccess(success);
        return result;
    }

    @ApiMethod(name = "bidOnProduct", httpMethod = "POST")
    public ResultBean bidOnProduct(@Named("user") String user, @Named("product_id") Long id,
                                   @Named("price") Integer price ) {
        ResultBean result = new ResultBean();

        Product p = ObjectifyService.ofy().load().type(Product.class).id(id).now();
        AuctionUser u = ObjectifyService.ofy().load().type(AuctionUser.class).id(user).now();

        Boolean success = false;
        if( p != null && u != null )
        {
            success = p.bid(user, price);
            if(success) {
                u.addBid(p.getId());
                ObjectifyService.ofy().save().entity( p ).now();
                ObjectifyService.ofy().save().entity( u ).now();
            }
        }

        result.setSuccess(success);
        return result;
    }

    @ApiMethod(name = "addProduct", httpMethod = "POST")
    public ResultBean addProduct(@Named("seller") String seller,
                                 @Named("name") String name,
                                 @Named("desc") String desc,
                                 @Named("price") Integer price,
                                 @Named("duration") Integer minutes)
    {
        ResultBean result = new ResultBean();

        Product p = new Product( seller, name, desc, price, minutes );

        ObjectifyService.ofy().save().entity( p ).now();
        result.setMessage(Long.toString(p.id));
        result.setSuccess(true);
        return result;
    }

    @ApiMethod(name = "setThumbnail", httpMethod = "POST")
    public ResultBean setThumbnail(@Named("product") Long id, ImageBean thumbnail)
    {
        ResultBean result = new ResultBean();

        Product p = ObjectifyService.ofy().load().type(Product.class).id(id).now();
        if (p != null) {
            p.addThumbnail(thumbnail.getImage());
            ObjectifyService.ofy().save().entity( p ).now();
            result.setSuccess(true);
        }
        return result;
    }

    @ApiMethod(name = "setImage", httpMethod = "POST")
    public ResultBean setImage(@Named("product") Long id, ImageBean image)
    {
        ResultBean result = new ResultBean();

        Product p = ObjectifyService.ofy().load().type(Product.class).id(id).now();

        if (p != null) {
            p.addPicture(image.getImage());
            ObjectifyService.ofy().save().entity(p).now();
            result.setSuccess(true);
        }

        return result;
    }

    @ApiMethod(name = "getNotifications", path="get_notifications", httpMethod = "GET")
    public NotificationsBean getNotifications(@Named("user") String user) {
        NotificationsBean response = new NotificationsBean();

        List<Notification> l = new ArrayList<>();
        l.addAll(ObjectifyService.ofy().load().type(Notification.class).filter("user ==", user).list());

        response.setData(l);
        return response;
    }

    @ApiMethod(name = "countNotifications", path="countNotifications", httpMethod = "GET")
    public ResultBean countNotifications(@Named("id") String user) {
        ResultBean result = new ResultBean();

        int s = ObjectifyService.ofy().load().type(Notification.class).filter("user ==", user).filter("read", false).list().size();

        Product.checkAllActive(ObjectifyService.ofy().load().type(Product.class).filter("active", true).list());

        result.setSuccess(true);
        result.setMessage(Integer.toString(s));
        return result;
    }

    @ApiMethod(name = "readNotification", path="read_notifications", httpMethod = "POST")
    public ResultBean readNotifications(@Named("id") long id) {
        ResultBean result = new ResultBean();

        Notification n = ObjectifyService.ofy().load().type(Notification.class).id(id).now();
        if (n != null) {
            n.read();
            ObjectifyService.ofy().save().entity(n).now();
            result.setSuccess(true);
        }

        return result;
    }

    @ApiMethod(name = "getTransactions", path="get_transactions", httpMethod = "GET")
    public TransactionsBean getTransactions(@Named("user") String user) {
        TransactionsBean response = new TransactionsBean();

        List<Transaction> l = new ArrayList<>();
        l.addAll(ObjectifyService.ofy().load().type(Transaction.class).filter("seller ==", user).list());
        l.addAll(ObjectifyService.ofy().load().type(Transaction.class).filter("buyer ==", user).list());

        response.setData(l);
        return response;
    }
}
