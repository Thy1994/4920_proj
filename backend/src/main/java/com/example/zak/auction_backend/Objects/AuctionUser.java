package com.example.zak.auction_backend.Objects;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

import java.util.ArrayList;
import java.util.List;

@Entity
public class AuctionUser {
    @Id public String name;
    @Index String email;

    public List<Long> bidList;
    public String hashed;

    public AuctionUser(){
    }

    public AuctionUser(String name, String hashed, String email)
    {
        this.name = name;
        this.hashed = hashed;
        this.email = email;
        this.bidList = new ArrayList<>();
    }

    public boolean verify(String hashed)
    {
        return this.hashed.equals(hashed);
    }

    public void changeEmail(String email)
    {
        this.email = email;
    }

    public String getEmail(){
        return this.email;
    }

    public void changePassword(String hashed)
    {
        this.hashed = hashed;
    }

    public void addBid(long product)
    {
        if( this.bidList == null)
        {
            this.bidList = new ArrayList<>();
        }
        this.bidList.add(product);
    }

    public void removeBid(long product)
    {
        if(bidList.contains(product))
        {
            bidList.remove(product);
        }
    }

    public List<Long> getBids(){
        if( this.bidList == null)
        {
            this.bidList = new ArrayList<>();
        }
        return this.bidList;
    }
}
