package com.example.zak.auction_backend.Beans;

public class ResultBean {
    private boolean mySuccess = false;
    private String myMessage = "";

    public ResultBean(){
        mySuccess = false;
    }

    public void setSuccess(boolean success) {
        mySuccess = success;
    }

    public void setMessage(String message) {
        myMessage = message;
    }

    public String getMessage() {
        return myMessage;
    }

    public boolean getData() {
        return mySuccess;
    }
}
