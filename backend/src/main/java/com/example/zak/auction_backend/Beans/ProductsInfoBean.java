package com.example.zak.auction_backend.Beans;

import com.example.zak.auction_backend.Objects.Product.ProductInfo;

import java.util.List;

/** The object model for the data we are sending through endpoints */
public class ProductsInfoBean {

    private List<ProductInfo> myData;

    public List<ProductInfo> getData() {
        return myData;
    }

    public void setData(List<ProductInfo> data) {
        myData = data;
    }
}