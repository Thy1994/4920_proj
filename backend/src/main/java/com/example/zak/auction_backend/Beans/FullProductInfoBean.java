package com.example.zak.auction_backend.Beans;

import com.example.zak.auction_backend.Objects.Product.FullProductInfo;

import java.util.List;

/** The object model for the data we are sending through endpoints */
public class FullProductInfoBean {
    private FullProductInfo myData;

    public FullProductInfo getData() {
        return myData;
    }

    public void setData(FullProductInfo data) {
        myData = data;
    }
}