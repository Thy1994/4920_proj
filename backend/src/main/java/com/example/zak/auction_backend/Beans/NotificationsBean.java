package com.example.zak.auction_backend.Beans;

import com.example.zak.auction_backend.Objects.Notification;

import java.util.List;

/** The object model for the data we are sending through endpoints */
public class NotificationsBean {

    private List<Notification> myData;

    public List<Notification> getData() {
        return myData;
    }

    public void setData(List<Notification> data) {
        myData = data;
    }
}