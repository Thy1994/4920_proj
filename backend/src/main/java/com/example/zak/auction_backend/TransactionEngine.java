package com.example.zak.auction_backend;

import com.example.zak.auction_backend.Objects.AuctionUser;
import com.example.zak.auction_backend.Objects.Notification;
import com.example.zak.auction_backend.Objects.Product;
import com.googlecode.objectify.ObjectifyService;

public class TransactionEngine {
    public static void completeTransaction(Product product){
        if(product.active){
            product.active = false;
            ObjectifyService.ofy().save().entity(product).now();

            Notification n;
            if(product.getBuyer() != null)
            {
                String s = product.getSeller();
                AuctionUser su = ObjectifyService.ofy().load().type(AuctionUser.class).id(s).now();
                String b = product.getBuyer();
                AuctionUser bu = ObjectifyService.ofy().load().type(AuctionUser.class).id(b).now();

                if( su != null && bu != null )
                {
                    n = new Notification(s, "You have sold \'" + product.getName() + "\' to user \'" +
                            b + "\' for " + product.getPrice()/100.0 + " dollars. Please contact " +
                            bu.getEmail() + " to arrange payment and delivery.");
                    ObjectifyService.ofy().save().entity(n).now();

                    bu.removeBid(product.getId());
                    n = new Notification(b, "You have bought \'" + product.getName() + "\' from user \'" +
                            s + "\' for " + product.getPrice()/100.0 + " dollars. Please contact " +
                            su.getEmail() + " to arrange payment and delivery.");
                    ObjectifyService.ofy().save().entity(n).now();

                    for(String bidder : product.getBidders())
                    {
                        if(bidder.equals(b)) continue;
                        AuctionUser u = ObjectifyService.ofy().load().type(AuctionUser.class).id(bidder).now();
                        if( u != null )
                        {
                            u.removeBid(product.getId());
                            n = new Notification(bidder, "You have failed to buy \'" + product.getName() + "\'.");
                            ObjectifyService.ofy().save().entity(n).now();
                        }
                    }
                }
            }
            else
            {
                String s = product.getSeller();
                AuctionUser su = ObjectifyService.ofy().load().type(AuctionUser.class).id(s).now();
                if( su != null )
                {
                    n = new Notification(s, "You have failed to sell \'" + product.getName() + "\'.");
                    ObjectifyService.ofy().save().entity(n).now();
                }
            }
        }
    }
}
