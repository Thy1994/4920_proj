package com.example.zak.auction_backend.Objects;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.condition.IfFalse;

import java.util.ArrayList;
import java.util.List;

@Entity
public class Notification {
    @Id public Long id;
    @Index String user;
    @Index boolean read;

    private String message;

    public Notification(){
        this.read = false;
        this.message = "";
    }

    public Notification(String user, String message){
        this.user = user;
        this.message = message;
        this.read = false;
    }

    public void setMessage(String message){
        this.message = message;
    }

    public String getMessage() {    return this.message;    }

    public void read()
    {
        this.read = true;
    }

    public boolean isRead(){
        return this.read;
    }

    public String getUser(){
        return this.user;
    }

    public static List<Notification> getUnread(List<Notification> notifications)
    {
        List<Notification> l = new ArrayList<>();
        for(Notification n : notifications){
            if(!n.isRead()) l.add(n);
        }
        return l;
    }
}
