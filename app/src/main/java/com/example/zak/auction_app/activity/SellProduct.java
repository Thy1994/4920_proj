package com.example.zak.auction_app.activity;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.ViewSwitcher;

import com.example.zak.auction_app.API.doProduct.addProductTask;
import com.example.zak.auction_app.R;
import com.example.zak.auction_app.utils.BitmapCodec;
import com.example.zak.auction_app.utils.SwipeDetector;

import java.util.ArrayList;
import java.util.Arrays;

public class SellProduct extends AppCompatActivity {
    private static final int REQUEST_IMAGE_CAPTURE = 1;
    private static final int RESULT_LOAD_IMG = 2;
    private static final int PIC_CROP = 3;
    private Uri picUri = null;
    private static final int pic_width = 300;
    private static final int pic_length = 300;
    private static final int tn_width = 50;
    private static final int tn_length = 50;
    int imageIndex = 0;
    ImageSwitcher is;

    boolean sell_active = true;

    String name ;
    String desc ;
    String price;
    int day;
    int hr;
    int min;
    ArrayList<Drawable> images;

    ArrayList<String> pictures;
    ArrayList<String> thumbnails;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sell_product);
        sell_active = true;

        images = new ArrayList<>();
        pictures = new ArrayList<>();
        thumbnails = new ArrayList<>();
        is = (ImageSwitcher) findViewById(R.id.imageSwitcher);
        //modify number picker
        modifyNumPicker();

        is = (ImageSwitcher) findViewById(R.id.imageSwitcher);
        is.setFactory(new ViewSwitcher.ViewFactory() {
            public View makeView() {
                ImageView myView = new ImageView(getApplicationContext());
                myView.setScaleType(ImageView.ScaleType.FIT_XY);
                FrameLayout.LayoutParams params = new ImageSwitcher.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT);
                myView.setLayoutParams(params);
                return myView;
            }
        });

        buildImageSwitcher();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }

    public void commitSell(View view){
        EditText nameField = (EditText) findViewById(R.id.name);
        EditText descField = (EditText) findViewById(R.id.description);
        EditText priceField = (EditText) findViewById(R.id.startingPrice);
        NumberPicker dayNp = (NumberPicker) findViewById(R.id.day_numPick);
        NumberPicker hrNp = (NumberPicker) findViewById(R.id.hour_numPick);
        NumberPicker minNp = (NumberPicker) findViewById(R.id.min_numPick);

        day = dayNp.getValue();
        hr = hrNp.getValue();
        min = minNp.getValue();

        name = nameField.getText().toString();
        desc = descField.getText().toString();
        price = priceField.getText().toString();

        //construct error message when product information inputs go wrong
        if( !checkProductInfo() ) return;
        if( !sell_active ) return;
        sell_active = false;

        //add the product to the backend
        boolean success;
        try
        {
            int minutes = day*24*60 + hr*60 + min;
            String userName = getIntent().getStringExtra("currUser");
            if(!images.isEmpty() && !thumbnails.isEmpty()){
                ArrayList<String> inputs = new ArrayList<>();
                inputs.addAll(Arrays.asList(userName, name, desc, price, Integer.toString(minutes)));
                inputs.add(Integer.toString(pictures.size()));
                inputs.add(thumbnails.get(0));
                inputs.addAll(pictures);
                success = new addProductTask().execute(inputs.toArray(new String[inputs.size()])).get();
            }
            else
            {
                success = new addProductTask().execute(userName, name, desc, price,
                        Integer.toString(minutes), "0").get();
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            success = false;
        }

        if( success ){
            finish();
        } else {
            //construct dialog builder
            sell_active = true;
            AlertDialog.Builder error = new AlertDialog.Builder(SellProduct.this);
            error.setPositiveButton("OK", new DialogInterface.OnClickListener()
                    {
                        public void onClick(DialogInterface dialog, int id) {

                        }
                    }
            );
            error.setTitle("Error");
            error.setMessage("Selling failed.");
            error.show();
        }
    }

    private boolean checkProductInfo(){
        int minutes = day*24*60 + hr*60 + min;
        //construct dialog builder
        AlertDialog.Builder error = new AlertDialog.Builder(SellProduct.this);
        error.setPositiveButton("OK", new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialog, int id) {

                    }
                }
        );
        error.setTitle("Error");

        //specify error message 5 condition: name,description,price,bid price,time
        if(minutes == 0){
            error.setMessage("Please enter a valid bidding time");
            error.show();
            return false;
        }

        if( name.equals("") ){
            error.setMessage("Please enter a product name.");
            error.show();
            return false;
        }

        if( desc.equals("") ){
            error.setMessage("Please enter a product description.");
            error.show();
            return false;
        }

        if( price.equals("") ){
            error.setMessage("Please enter a price.");
            error.show();
            return false;
        }

        if( Double.valueOf( price ) > 20000000.00 ) {
            error.setMessage("Cannot sell items over 20 million dollars.");
            error.show();
            return false;
        }

        if(minutes<=0){
            error.setMessage("Please enter an auction duration.");
            error.show();
            return false;
        }

        return true;
    }

    private void modifyNumPicker(){
        NumberPicker dayNp = (NumberPicker) findViewById(R.id.day_numPick);
        NumberPicker hrNp = (NumberPicker) findViewById(R.id.hour_numPick);
        NumberPicker minNp = (NumberPicker) findViewById(R.id.min_numPick);

        dayNp.setMaxValue(365);
        hrNp.setMaxValue(23);
        minNp.setMaxValue(59);
    }


    //camera function magic dont touch

    public void clickImage(View view){
        dispatchTakePictureIntent();
    }

    public void clickChoose(View view){
        loadImagefromGallery();
    }


    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }

    private void loadImagefromGallery() {
        // Create intent to Open Image applications like Gallery, Google Photos
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        // Start the Intent
        startActivityForResult(galleryIntent, RESULT_LOAD_IMG);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode,resultCode,data);

        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            picUri = data.getData();
        }

        if (requestCode == RESULT_LOAD_IMG && resultCode == Activity.RESULT_OK) {
            picUri = data.getData();
        }

        if (requestCode == PIC_CROP && resultCode == Activity.RESULT_OK) {
            //get the returned data
            Bundle extras = data.getExtras();
            //get the cropped bitmap
            Bitmap thePic = extras.getParcelable("data");

            Bitmap picBm = Bitmap.createScaledBitmap(thePic,pic_width,pic_length, true);
            Bitmap thumbBm = Bitmap.createScaledBitmap(thePic,tn_width,tn_length, true);
            images.add(new BitmapDrawable(picBm));

            pictures.add(BitmapCodec.BitMapToString(picBm));
            thumbnails.add(BitmapCodec.BitMapToString(thumbBm));

            buildImageSwitcher();

            return;
        }

        if(picUri != null){
            performCrop();
            picUri = null;
        }
    }

    private void buildImageSwitcher(){
        if(images.size() == 0) return;
        imageIndex = 0;

        is.setImageDrawable(images.get(imageIndex));

        is.setOnTouchListener(new SwipeDetector(is) {
            @Override
            public void onLeftToRightSwipe() {
                imageIndex = ((images.size()+imageIndex)+1)%(images.size());
                is.setImageDrawable(images.get(imageIndex%(images.size())));
            }
            @Override
            public void onRightToLeftSwipe() {
                imageIndex = ((images.size()+imageIndex)-1)%(images.size());
                is.setImageDrawable(images.get(imageIndex%(images.size())));
            }
        });
    }

    private void performCrop(){
        Intent cropIntent = new Intent("com.android.camera.action.CROP");
        //indicate image type and Uri
        cropIntent.setDataAndType(picUri, "image/*");
        //set crop properties
        cropIntent.putExtra("crop", "true");
        //indicate aspect of desired crop
        cropIntent.putExtra("aspectX", 1);
        cropIntent.putExtra("aspectY", 1);
        //indicate output X and Y
        cropIntent.putExtra("outputX", 256);
        cropIntent.putExtra("outputY", 256);
        //retrieve data on return
        cropIntent.putExtra("return-data", true);
        //start the activity - we handle returning in onActivityResult
        startActivityForResult(cropIntent, PIC_CROP);
    }

    public void cancelSell(View view) {
        finish();
    }

}