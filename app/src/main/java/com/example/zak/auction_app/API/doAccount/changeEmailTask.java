package com.example.zak.auction_app.API.doAccount;

import android.os.AsyncTask;

import com.example.zak.auction_app.API.APIService;

import java.io.IOException;

public class changeEmailTask extends AsyncTask<String, Void, Boolean> {

    @Override
    protected Boolean doInBackground(String... params) {
        String user = params[0];
        String email = params[1];

        try {
            return APIService.getApi().changeEmail(user,email).execute().getData();
        } catch (IOException  e) {
            e.printStackTrace();
            return false;
        }
    }
}