package com.example.zak.auction_app.API;

import com.example.zak.auction_backend.auctionAppApi.AuctionAppApi;
import com.example.zak.auction_backend.auctionAppApi.AuctionAppApi.Builder;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.extensions.android.json.AndroidJsonFactory;
import com.google.api.client.googleapis.services.AbstractGoogleClientRequest;
import com.google.api.client.googleapis.services.GoogleClientRequestInitializer;

import java.io.IOException;

public class APIService {
    private final static boolean LOCAL = false;
    private final static String LOCAL_IP = "10.0.2.2:8080";
    private final static String REMOTE_IP = "selling-point-test.appspot.com";

    private static AuctionAppApi myApiService;

    public static AuctionAppApi getApi()
    {
        if( myApiService == null ) {  // Only do this once
            Builder builder = new AuctionAppApi.Builder(AndroidHttp.newCompatibleTransport(),
                            new AndroidJsonFactory(), null);
            if( LOCAL ){
                builder.setRootUrl("http://" + LOCAL_IP + "/_ah/api/");
                builder.setGoogleClientRequestInitializer(new GoogleClientRequestInitializer() {
                    @Override
                    public void initialize(AbstractGoogleClientRequest<?> abstractGoogleClientRequest) throws IOException {
                        abstractGoogleClientRequest.setDisableGZipContent(true);
                    }
                });
            } else {
                builder.setRootUrl("https://" + REMOTE_IP + "/_ah/api/");
            }
            myApiService = builder.build();

        }
        return myApiService;
    }

    public static String getAddress(){
        if(LOCAL) return LOCAL_IP;
        return REMOTE_IP;
    }
}
