package com.example.zak.auction_app.utils;

import java.text.DecimalFormat;

public class Price {
    public static int toInt(double dollars) {
        return ( (Double) Math.floor(dollars * 100) ).intValue();
    }

    public static double toDouble(int cents){
        return cents/100.0 ;
    }

    public static String toString(double dollars) {
        DecimalFormat format = new DecimalFormat("#,##0.00");
        return "$" + format.format(dollars);
    }

    public static String toString(int cents) {
        return toString( ((Integer) cents).doubleValue() / 100.0 );
    }

    public static String toShortString(double dollars) {
        String[] size = {"  ", "K", "M", "B", "T"};
        int index = 0;

        while( dollars >= 1000 ){
            dollars/=1000;
            index++;
        }
        DecimalFormat format = new DecimalFormat("#.00");
        return String.format("%7s", "$" + format.format(dollars) + size[index]);
    }

    public static String toShortString(int cents) {
        return toShortString( ((Integer) cents).doubleValue() / 100.0 );
    }
}
