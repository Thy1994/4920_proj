package com.example.zak.auction_app.activity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.example.zak.auction_app.API.doAccount.changeEmailTask;
import com.example.zak.auction_app.API.doAccount.changePasswordTask;
import com.example.zak.auction_app.API.doAccount.getEmailTask;
import com.example.zak.auction_app.R;

import java.util.concurrent.ExecutionException;

public class ViewAccount extends AppCompatActivity {
    String userName;
    String oldPassword;
    String newPassword;
    String confirmPassword;
    String newEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.account);
        userName = getIntent().getStringExtra("currUser");

        ( (TextView) findViewById(R.id.user_display) ).setText(userName);

        String email = "";
        try {
            email = new getEmailTask().execute(userName).get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        ( (TextView) findViewById(R.id.email_edit) ).setText(email);
    }

    private boolean checkInputs(){

        //construct dialog builder
        AlertDialog.Builder error = new AlertDialog.Builder(ViewAccount.this);
        error.setPositiveButton("OK", new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialog, int id) {

                    }
                }
        );
        error.setTitle("Error");

        if( newEmail.equals("") )
        {
            error.setMessage("Please enter an email address");
            error.show();
            return false;
        }
        else if ( !newEmail.matches("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$") )
        {
            error.setMessage("Please enter a valid email address");
            error.show();
            return false;
        }

        return true;
    }


    public void changeEmail(View view){
        final EditText newEmailField = (EditText) findViewById(R.id.email_edit);
        newEmail = newEmailField.getText().toString();
        if (!checkInputs()) return;

        {
            boolean success;
            try {
                success = new changeEmailTask().execute(userName,newEmail).get();
            } catch (Exception e) {
                success = false;
                e.printStackTrace();
            }

            if (success) {
                AlertDialog.Builder error = new AlertDialog.Builder(ViewAccount.this);
                error.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                            }
                        }
                );
                error.setMessage("Email Change Successful");
                error.show();
            } else {


                //construct dialog builder
                AlertDialog.Builder error = new AlertDialog.Builder(ViewAccount.this);
                error.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                            }
                        }
                );
                error.setMessage("Error");
                error.show();
            }
        }


    }


    public void changePassword(View view){
        // String confirmPassword, String newPassword
        final EditText oldPasswordField = (EditText) findViewById(R.id.password_edit);
        final EditText newPasswordField = (EditText) findViewById(R.id.new_password_edit);
        final EditText confirmPasswordField = (EditText) findViewById(R.id.confirm_password_edit);

        oldPassword = oldPasswordField.getText().toString();
        newPassword = newPasswordField.getText().toString();
        confirmPassword = confirmPasswordField.getText().toString();

        if (!checkPasswords()) return;

        boolean success;
        try
        {
            success = new changePasswordTask().execute(userName, oldPassword, newPassword).get();
        }
        catch (Exception e)
        {
            success = false;
            e.printStackTrace();
        }

        if( success ){
            AlertDialog.Builder error = new AlertDialog.Builder(ViewAccount.this);
            error.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {

                        }
                    }
            );
            error.setMessage("Password Change Successful");
            error.show();
        }
        else
        {
            //construct dialog builder
            AlertDialog.Builder error = new AlertDialog.Builder(ViewAccount.this);
            error.setPositiveButton("OK", new DialogInterface.OnClickListener()
                    {
                        public void onClick(DialogInterface dialog, int id) {

                        }
                    }
            );
            error.setTitle("Error");
            error.setMessage("Password change failed.");
            error.show();
        }
    }

    public void cancel(View view){
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }


    private boolean checkPasswords(){

        //construct dialog builder
        AlertDialog.Builder error = new AlertDialog.Builder(ViewAccount.this);
        error.setPositiveButton("OK", new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialog, int id) {

                    }
                }
        );
        error.setTitle("Error");

        //specify error message 4 condition: name,description,price,bid price
        if( oldPassword.equals("") ){
            error.setMessage("Please enter your old password.");
            error.show();
            return false;
        }

        if( newPassword.equals("") )
        {
            error.setMessage("Please enter a new password");
            error.show();
            return false;
        }
        else if( !newPassword.matches("[A-Za-z0-9\\.,_\\-\\?!]+") )
        {
            error.setMessage("New password can only contain alphanumeric characters and the following characters: .,_-?!");
            error.show();
            return false;
        }
        else if( newPassword.length() < 6 || newPassword.length() > 30 )
        {
            error.setMessage("New password must be longer than 6 characters.");
            error.show();
            return false;
        }

        if( !newPassword.equals(confirmPassword) ){
            error.setMessage("New passwords do not match");
            error.show();
            return false;
        }


        return true;
    }
//
//    public void toggleCardInfo(View view){
//
//
//
//        RelativeLayout card = (RelativeLayout) findViewById(R.id.credit_card_layout);
//        if(card.getVisibility() == View.VISIBLE){
//            card.setVisibility(View.INVISIBLE);
//
//        }else {
//            card.setVisibility(View.VISIBLE);
//        }
//    }
//    public void toggleUserInfo(View view){
//        RelativeLayout user = (RelativeLayout) findViewById(R.id.user_layout);
//        if(user.getVisibility() == View.VISIBLE){
//            user.setVisibility(View.INVISIBLE);
//
//            //when toggle, toggle_userInfo should be below toggle_user_info button
//            Button cardBtn = (Button) findViewById(R.id.toggle_cardInfo);
//
//            //create rule and apply
//            RelativeLayout.LayoutParams p = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
//                    ViewGroup.LayoutParams.WRAP_CONTENT);
//            p.addRule(RelativeLayout.BELOW, R.id.toggle_userInfo);
//
//            cardBtn.setLayoutParams(p);
//
//        }else {
//            user.setVisibility(View.VISIBLE);
//
//            //when toggle, toggle_userInfo should be below toggle_user_info button
//            Button cardBtn = (Button) findViewById(R.id.toggle_cardInfo);
//
//            //create rule and apply
//            RelativeLayout.LayoutParams p
// = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
//                    ViewGroup.LayoutParams.WRAP_CONTENT);
//            p.addRule(RelativeLayout.BELOW, R.id.user_layout);
//
//            cardBtn.setLayoutParams(p);
//        }
//    }
}
