package com.example.zak.auction_app.API.retrieveLists;

import android.os.AsyncTask;

import com.example.zak.auction_app.API.APIService;
import com.example.zak.auction_backend.auctionAppApi.model.ProductInfo;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class getBidsTask extends AsyncTask<String, Void, List<ProductInfo>> {
    @Override
    protected List<ProductInfo> doInBackground(String... params) {
        String user = params[0];

        try
        {
            return APIService.getApi().getBids(user).execute().getData();
        }
        catch (IOException e)
        {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }
}