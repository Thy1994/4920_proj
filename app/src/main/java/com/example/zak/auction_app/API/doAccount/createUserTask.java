package com.example.zak.auction_app.API.doAccount;

import android.os.AsyncTask;
import android.util.Base64;

import com.example.zak.auction_app.API.APIService;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.security.GeneralSecurityException;
import java.security.MessageDigest;
import java.security.PublicKey;

import javax.crypto.Cipher;

public class createUserTask extends AsyncTask<String, Void, Boolean> {
    @Override
    protected Boolean doInBackground(String... params) {
        String user = params[0];
        String pass = params[1];
        String email = params[2];

        try
        {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] hashed = md.digest( pass.getBytes("UTF-8") );

            byte[] keyBytes = Base64.decode(APIService.getApi().requestKey(user).execute().getData(), Base64.DEFAULT);
            PublicKey key = (PublicKey) new ObjectInputStream(new ByteArrayInputStream(keyBytes)).readObject();

            Cipher encrypt = Cipher.getInstance("RSA/ECB/PKCS1Padding");
            encrypt.init(Cipher.ENCRYPT_MODE, key);
            String encrypted = Base64.encodeToString( encrypt.doFinal(hashed), Base64.DEFAULT );

            return APIService.getApi().createUser(user, encrypted, email).execute().getData();
        }
        catch (IOException | ClassNotFoundException | GeneralSecurityException e)
        {
            e.printStackTrace();
            return false;
        }
    }
}