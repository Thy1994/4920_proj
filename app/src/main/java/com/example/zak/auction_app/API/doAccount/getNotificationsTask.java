package com.example.zak.auction_app.API.doAccount;

import android.os.AsyncTask;

import com.example.zak.auction_app.API.APIService;
import com.example.zak.auction_backend.auctionAppApi.model.Notification;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class getNotificationsTask extends AsyncTask<String, Void, List<Notification>> {
    @Override
    protected List<Notification> doInBackground(String... params) {
        String user = params[0];

        try {
            return APIService.getApi().getNotifications(user).execute().getData();
        } catch (IOException e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }
}