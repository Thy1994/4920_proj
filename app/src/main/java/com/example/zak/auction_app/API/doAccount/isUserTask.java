package com.example.zak.auction_app.API.doAccount;

import android.os.AsyncTask;

import com.example.zak.auction_app.API.APIService;

import java.io.IOException;

public class isUserTask extends AsyncTask<String, Void, Boolean> {
    @Override
    protected Boolean doInBackground(String... params) {
        String user = params[0];

        try {
            return APIService.getApi().isUser(user).execute().getData();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }
}