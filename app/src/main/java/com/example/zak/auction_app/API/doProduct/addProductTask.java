package com.example.zak.auction_app.API.doProduct;

import android.os.AsyncTask;

import com.example.zak.auction_app.API.APIService;
import com.example.zak.auction_app.utils.Price;
import com.example.zak.auction_backend.auctionAppApi.AuctionAppApi;
import com.example.zak.auction_backend.auctionAppApi.model.ImageBean;
import com.example.zak.auction_backend.auctionAppApi.model.ResultBean;

import java.io.IOException;
import java.util.Arrays;

public class addProductTask extends AsyncTask<String, Void, Boolean> {
    @Override
    protected Boolean doInBackground(String... params) {
        String user = params[0];
        String name = params[1];
        String desc = params[2];
        int price = Price.toInt( Double.parseDouble( params[3] ) );
        int minutes = Integer.parseInt( params[4] );
        int imageCount = Integer.parseInt(params[5]);

        System.out.println(Arrays.asList(user, name, desc, price, minutes));

        try {
            AuctionAppApi api = APIService.getApi();

            ResultBean result = api.addProduct(user, name, desc, price, minutes).execute();
            System.out.println(result);

            if(imageCount > 0)
            {
                long p_id = Long.valueOf(result.getMessage());
                String thumbnail = params[6];

                api.setThumbnail(p_id, new ImageBean().setImage(thumbnail)).execute();
                for( String image : Arrays.asList(params).subList(7, 7+imageCount) ) {
                    api.setImage(p_id, new ImageBean().setImage(image)).execute();
                }
            }

            return result.getData();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }
}