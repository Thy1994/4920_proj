package com.example.zak.auction_app.activity;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.zak.auction_app.R;
import com.example.zak.auction_app.utils.BitmapCodec;
import com.example.zak.auction_app.utils.Price;
import com.example.zak.auction_backend.auctionAppApi.model.ProductInfo;


public class ProductArrayAdapter extends ArrayAdapter<ProductInfo> {

    public ProductArrayAdapter(Context context, int resource, ProductInfo[] objects) {
        super(context, resource, objects);
    }

    @NonNull
    @Override
    public View getView(int position, View view, @NonNull ViewGroup parent ) {

        View row = view;

        if(row == null)
        {
            LayoutInflater inflater = ( (Activity) getContext() ).getLayoutInflater();
            row = inflater.inflate(R.layout.product_view, parent, false);
        }

        final ProductInfo product = getItem(position);

        if(product != null)
        {
            ( (TextView) row.findViewById(R.id.price) ).setText(Price.toShortString(product.getPrice()));
            ( (TextView) row.findViewById(R.id.name) ).setText(product.getName());
            String tn = product.getThumbnail();
            if(tn == null || tn.equals("") || tn.equals("-"))
            {
                ((ImageView) row.findViewById(R.id.thumbnail)).setImageResource(
                        R.drawable.ic_visibility_off_black_48dp);
            } else {
                ((ImageView) row.findViewById(R.id.thumbnail)).setImageBitmap(
                        BitmapCodec.StringToBitMap(product.getThumbnail())
                );
            }
        }
        else
        {
            ( (TextView) row.findViewById(R.id.price) ).setText("???");
            ( (TextView) row.findViewById(R.id.name) ).setText("???");
        }


        return row;
    }
}
