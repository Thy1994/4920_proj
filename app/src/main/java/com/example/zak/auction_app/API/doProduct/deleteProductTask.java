package com.example.zak.auction_app.API.doProduct;

import android.os.AsyncTask;

import com.example.zak.auction_app.API.APIService;

import java.io.IOException;

public class deleteProductTask extends AsyncTask<Long, Void, Boolean> {
    @Override
    protected Boolean doInBackground(Long... params) {
        long id = params[0];

        try
        {
            return APIService.getApi().deleteProduct(id).execute().getData();
        }
        catch (IOException e)
        {
            e.printStackTrace();
            return false;
        }
    }
}