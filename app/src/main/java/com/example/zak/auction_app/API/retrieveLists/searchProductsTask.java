package com.example.zak.auction_app.API.retrieveLists;

import android.os.AsyncTask;

import com.example.zak.auction_app.API.APIService;
import com.example.zak.auction_backend.auctionAppApi.model.ProductInfo;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class searchProductsTask extends AsyncTask<String, Void, List<ProductInfo>> {
    @Override
    protected List<ProductInfo> doInBackground(String... params) {
        String user = params[0];
        String search = params[1];

        try
        {
            return APIService.getApi().searchProducts(search, user).execute().getData();
        }
        catch (IOException e)
        {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }
}