package com.example.zak.auction_app.API.doAccount;

import android.os.AsyncTask;

import com.example.zak.auction_app.API.APIService;

import java.io.IOException;

public class countNotificationsTask extends AsyncTask<String, Void, Integer> {
    @Override
    protected Integer doInBackground(String... params) {
        String user = params[0];

        try {
            return Integer.parseInt(APIService.getApi().countNotifications(user).execute().getMessage());
        } catch (IOException e) {
            e.printStackTrace();
            return 0;
        }
    }
}