package com.example.zak.auction_app.activity;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.example.zak.auction_app.API.doAccount.countNotificationsTask;
import com.example.zak.auction_app.API.doAccount.getNotificationsTask;
import com.example.zak.auction_app.API.retrieveLists.getBidsTask;
import com.example.zak.auction_app.API.retrieveLists.getProductsByOthersTask;
import com.example.zak.auction_app.API.retrieveLists.getProductsByUserTask;
import com.example.zak.auction_app.API.retrieveLists.searchProductsTask;
import com.example.zak.auction_app.R;
import com.example.zak.auction_backend.auctionAppApi.model.Notification;
import com.example.zak.auction_backend.auctionAppApi.model.ProductInfo;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class ViewProducts extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    DrawerLayout drawerLayout;
    Intent intent;
    ListView list;
    String currUser;
    String productListType;

    // Toolbar variables
    Toolbar toolbar;

    // Side menu variables
    ActionBarDrawerToggle sideMenuToggle;
    NavigationView sideMenuView;
    TextView sideMenuHeaderTitle;
    TextView sideMenuHeaderNotifications;
    View sideMenuHeader;

    Integer notificationCount = 0;

    // Search function variables
    MenuItem searchButtonAndBar;
    SearchView searchView;
    String searchInput;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_items, menu);

        searchButtonAndBar = menu.findItem(R.id.searchButtonAndBar);
        searchView = (SearchView) MenuItemCompat.getActionView(searchButtonAndBar);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                //Toast.makeText(ViewProducts.this, query, Toast.LENGTH_SHORT).show();
                searchInput = query;
                productListType = "userSearch";
                buildProductList();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.buy_product_list);

        drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        list = (ListView) findViewById(R.id.productListView);
        currUser = getIntent().getStringExtra("currUser");
        productListType = "default";

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        // Set up side menu
        sideMenuView = (NavigationView) findViewById(R.id.sideMenu);
        sideMenuView.setNavigationItemSelectedListener(this);

        sideMenuToggle = new ActionBarDrawerToggle(
                this,
                drawerLayout,
                toolbar,
                R.string.side_menu_open,
                R.string.side_menu_close) {

            // When side menu opens, close keyboard
            public void onDrawerOpened(View drawerView) {
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(drawerLayout.getWindowToken(), 0);
                countNotifications();
            }


            public void onDrawerClosed(View view) {
            }
        };

        drawerLayout.setDrawerListener(sideMenuToggle);
        sideMenuToggle.syncState();

        sideMenuHeader = LayoutInflater.from(this).inflate(R.layout.side_menu_header, null);
        sideMenuView.addHeaderView(sideMenuHeader);
        sideMenuHeaderTitle = (TextView) sideMenuHeader.findViewById(R.id.sideMenuHeaderTitle);
        sideMenuHeaderTitle.setText(currUser);
        sideMenuHeaderNotifications = (TextView) sideMenuHeader.findViewById(R.id.sideMenuHeaderNotifications);
        sideMenuHeaderNotifications.setText(String.format(getResources().getString(R.string.notificationCount), notificationCount));
    }

    @Override
    public void onResume() {
        super.onResume();
        buildProductList();
        countNotifications();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }

    private void countNotifications() {
        try {
            notificationCount = new countNotificationsTask().execute(currUser).get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        if (sideMenuHeaderNotifications != null) {
            sideMenuHeaderNotifications.setText(
                    String.format(getResources().getString(R.string.notificationCount),
                            notificationCount));
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.sideMenuHome:
                productListType = "default";
                buildProductList();
                break;
            case R.id.sideMenuAuctions:
                productListType = "userAuctions";
                buildProductList();
                break;
            case R.id.sideMenuBids:
                productListType = "userBids";
                buildProductList();
                break;
            case R.id.sideMenuHeaderNotifications:
                productListType = "notifications";
                buildProductList();
                break;
            case R.id.sideMenuSell:
                intent = new Intent(ViewProducts.this, SellProduct.class);
                intent.putExtra("currUser", currUser);
                startActivity(intent);
                break;
            case R.id.sideMenuAccount:
                intent = new Intent(ViewProducts.this, ViewAccount.class);
                intent.putExtra("currUser", currUser);
                startActivity(intent);
                break;
            case R.id.sideMenuLogOut:
                intent = new Intent(ViewProducts.this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                break;
        }

        drawerLayout.closeDrawers();
        return true;
    }

    private void buildProductList() {
        List<ProductInfo> products = null;
        try {
            switch (productListType) {
                case "default":
                    ((TextView) findViewById(R.id.productTitle)).setText(R.string.products_for_sale);
                    products = new getProductsByOthersTask().execute(currUser).get();
                    break;
                case "userAuctions":
                    ((TextView) findViewById(R.id.productTitle)).setText(R.string.your_auctions);
                    products = new getProductsByUserTask().execute(currUser).get();
                    break;
                case "userBids":
                    ((TextView) findViewById(R.id.productTitle)).setText(R.string.your_bids);
                    products = new getBidsTask().execute(currUser).get();
                    break;
                case "userSearch":
                    ((TextView) findViewById(R.id.productTitle)).setText(R.string.search_results);
                    products = new searchProductsTask().execute(currUser, searchInput).get();
                    break;
                case "notifications":
                    ((TextView) findViewById(R.id.productTitle)).setText(R.string.notifications);
                    buildNotificationsList();
                    return;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if(products == null) products = new ArrayList<>();

        //setup product list
        ArrayAdapter<ProductInfo> adapter = new ProductArrayAdapter(this, R.layout.product_view,
                products.toArray(new ProductInfo[products.size()]));

        list.setAdapter(adapter);

        //when click on item it triggers new activity
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                ProductInfo item = (ProductInfo) list.getItemAtPosition(i);

                if (item.getSeller().equals(currUser)) {
                    intent = new Intent(ViewProducts.this, OwnProduct.class);
                } else {
                    intent = new Intent(ViewProducts.this, BidProduct.class);
                }

                //get product ID from the list
                intent.putExtra("productID", item.getId());
                intent.putExtra("currUser", currUser);
                startActivity(intent);
            }
        });
    }


    private void buildNotificationsList() {
        List<Notification> notifications = null;
        try {
            notifications = new getNotificationsTask().execute(currUser).get();
        } catch (Exception e) {
            e.printStackTrace();
        }

        if(notifications == null) notifications = new ArrayList<>();

        System.out.println(notifications.size() + " notifications");

        //setup product list
        ArrayAdapter<Notification> adapter = new NotificationArrayAdapter(this, R.layout.notification_view,
                notifications.toArray(new Notification[notifications.size()]));

        list.setAdapter(adapter);

        //when click on item it triggers new activity
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                Notification item = (Notification) list.getItemAtPosition(i);
                intent = new Intent(ViewProducts.this, ReadNotification.class);

                intent.putExtra("note_id", item.getId());
                intent.putExtra("message", item.getMessage());
                intent.putExtra("currUser", currUser);
                startActivity(intent);
            }
        });
    }

    public void startSellProduct (View view) {
        intent = new Intent(ViewProducts.this, SellProduct.class);
        intent.putExtra("currUser", currUser);

        startActivity(intent);
    }
}