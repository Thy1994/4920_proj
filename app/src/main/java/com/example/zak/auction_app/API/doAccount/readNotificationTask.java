package com.example.zak.auction_app.API.doAccount;

import android.os.AsyncTask;

import com.example.zak.auction_app.API.APIService;

import java.io.IOException;

public class readNotificationTask extends AsyncTask<String, Void, Boolean> {
    @Override
    protected Boolean doInBackground(String... params) {
        Long note_id = Long.parseLong(params[0]);

        try {
            return APIService.getApi().readNotification(note_id).execute().getData();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }
}