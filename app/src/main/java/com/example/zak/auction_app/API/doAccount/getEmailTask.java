package com.example.zak.auction_app.API.doAccount;

import android.os.AsyncTask;

import com.example.zak.auction_app.API.APIService;

import java.io.IOException;

public class getEmailTask extends AsyncTask<String, Void, String> {
    @Override
    protected String doInBackground(String... params) {
        String user = params[0];

        try {
            return APIService.getApi().getEmail(user).execute().getMessage();
        } catch (IOException e) {
            e.printStackTrace();
            return "not found";
        }
    }
}