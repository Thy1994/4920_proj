package com.example.zak.auction_app.API.doProduct;

import android.os.AsyncTask;

import com.example.zak.auction_app.API.APIService;
import com.example.zak.auction_app.utils.Price;

import java.io.IOException;

public class bidOnProductTask extends AsyncTask<String, Void, Boolean> {
    @Override
    protected Boolean doInBackground(String... params) {
        String user = params[0];
        Long product_id = Long.parseLong(params[1]);
        Integer price = Price.toInt( Double.parseDouble( params[2] ) );

        try {
            return APIService.getApi().bidOnProduct(user, product_id, price).execute().getData();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }
}