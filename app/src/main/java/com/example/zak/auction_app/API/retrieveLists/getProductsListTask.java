package com.example.zak.auction_app.API.retrieveLists;

import android.os.AsyncTask;

import com.example.zak.auction_app.API.APIService;
import com.example.zak.auction_backend.auctionAppApi.model.ProductInfo;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class getProductsListTask extends AsyncTask<Void, Void, List<ProductInfo>> {
    @Override
    protected List<ProductInfo> doInBackground(Void... params) {
        try
        {
            return APIService.getApi().getProducts().execute().getData();
        }
        catch (IOException e)
        {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }
}