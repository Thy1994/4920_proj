package com.example.zak.auction_app.activity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

import com.example.zak.auction_app.API.doAccount.createUserTask;
import com.example.zak.auction_app.API.doAccount.isUserTask;
import com.example.zak.auction_app.R;

import java.util.concurrent.ExecutionException;

public class RegisterAccount extends AppCompatActivity {
    String username;
    String password;
    String confirmPassword;
    String email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }

    public void cancel(View view){
        finish();
    }


    private boolean checkInputs(){

        //construct dialog builder
        AlertDialog.Builder error = new AlertDialog.Builder(RegisterAccount.this);
        error.setPositiveButton("OK", new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialog, int id) {

                    }
                }
        );
        error.setTitle("Error");

        if( username.equals("") )
        {
            error.setMessage("Please enter a username.");
            error.show();
            return false;
        }
        else if( !username.matches("[A-Za-z0-9_]+") )
        {
            error.setMessage("Username can only contain alphanumeric characters and underscores");
            error.show();
            return false;
        }
        else if( username.length() < 4 || username.length() > 16 )
        {
            error.setMessage("Username must be between 4 and 16 characters characters.");
            error.show();
            return false;
        }
        else
        {
            boolean taken;
            try
            {
                taken = new isUserTask().execute(username).get();
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
                return false;
            }
            if (taken) {
                error.setMessage("Username is already taken");
                error.show();
                return false;
            }
        }

        if( password.equals("") )
        {
            error.setMessage("Please enter a password");
            error.show();
            return false;
        }
        else if( !password.matches("[A-Za-z0-9\\.,_\\-\\?!]+") )
        {
            error.setMessage("Password can only contain alphanumeric characters and the following characters: .,_-?!");
            error.show();
            return false;
        }
        else if( password.length() < 6 || password.length() > 30 )
        {
            error.setMessage("Password must be between 6 and 30 characters.");
            error.show();
            return false;
        }

        if( !password.equals(confirmPassword) ){
            error.setMessage("Passwords do not match");
            error.show();
            return false;
        }

        if( email.equals("") )
        {
            error.setMessage("Please enter an email address");
            error.show();
            return false;
        }
        else if ( !email.matches("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$") )
        {
            error.setMessage("Please enter a valid email address");
            error.show();
            return false;
        }

        return true;
    }

    public void commitRegister(View view){
        final EditText usernameField = (EditText) findViewById(R.id.username_edit);
        final EditText passwordField = (EditText) findViewById(R.id.password_edit);
        final EditText confirmPasswordField = (EditText) findViewById(R.id.confirm_password_edit);
        final EditText emailField = (EditText) findViewById(R.id.email_edit);

        username = usernameField.getText().toString();
        password = passwordField.getText().toString();
        confirmPassword = confirmPasswordField.getText().toString();
        email = emailField.getText().toString();

        if ( !checkInputs() ) return;

        boolean success;
        try
        {
            success = new createUserTask().execute(username, password, email).get();
        }
        catch (Exception e)
        {
            e.printStackTrace();
            success = false;
        }

        if( success ){
            finish();
        } else {
            AlertDialog.Builder error = new AlertDialog.Builder(RegisterAccount.this);
            error.setPositiveButton("OK", new DialogInterface.OnClickListener()
                    {
                        public void onClick(DialogInterface dialog, int id) {

                        }
                    }
            );
            error.setTitle("Error");
            error.setMessage("Account creation failed.");
            error.show();
        }
    }
}
