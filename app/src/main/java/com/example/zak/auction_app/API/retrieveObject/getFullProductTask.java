package com.example.zak.auction_app.API.retrieveObject;

import android.os.AsyncTask;

import com.example.zak.auction_app.API.APIService;
import com.example.zak.auction_backend.auctionAppApi.model.FullProductInfo;

import java.io.IOException;

public class getFullProductTask extends AsyncTask<Long, Void, FullProductInfo> {
    @Override
    protected FullProductInfo doInBackground(Long... params) {
        long id = params[0];

        try
        {
            return APIService.getApi().getFullProduct(id).execute().getData();
        }
        catch (IOException e)
        {
            e.printStackTrace();
            return null;
        }
    }
}