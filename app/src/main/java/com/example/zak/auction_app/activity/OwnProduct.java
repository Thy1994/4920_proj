package com.example.zak.auction_app.activity;

import android.content.DialogInterface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import com.example.zak.auction_app.API.doProduct.deleteProductTask;
import com.example.zak.auction_app.API.retrieveObject.getFullProductTask;
import com.example.zak.auction_app.R;
import com.example.zak.auction_app.utils.BitmapCodec;
import com.example.zak.auction_app.utils.Price;
import com.example.zak.auction_app.utils.SwipeDetector;
import com.example.zak.auction_backend.auctionAppApi.model.FullProductInfo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class OwnProduct extends AppCompatActivity {
    ImageSwitcher is;
    int imageIndex;
    List<Drawable> images;
    long product_id = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.own_product);

        product_id = this.getIntent().getLongExtra("productID",0);
        images = new ArrayList<>();

        is = (ImageSwitcher) findViewById(R.id.imageSwitcher);
        is.setFactory(new ViewSwitcher.ViewFactory() {
            public View makeView() {
                ImageView myView = new ImageView(getApplicationContext());
                myView.setScaleType(ImageView.ScaleType.FIT_XY);
                FrameLayout.LayoutParams params = new ImageSwitcher.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT);
                myView.setLayoutParams(params);
                return myView;
            }
        });
        displayProduct();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }

    // TODO: 6/10/2016
    //fill product information according to product_id
    private void displayProduct(){
        if(product_id == 0){
            ( (TextView) findViewById(R.id.description_contents) ).setText(R.string.not_found);
            ( (TextView) findViewById(R.id.name_contents) ).setText(R.string.not_found);
            ( (TextView) findViewById(R.id.price_contents) ).setText(R.string.not_found);
            ( (TextView) findViewById(R.id.expiry) ).setText(R.string.not_found);
        }
        else
        {
            FullProductInfo product = null;
            try {
                product = new getFullProductTask().execute(product_id).get();
            } catch (Exception e) {
                e.printStackTrace();
            }

//            DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");

            if( product != null )
            {
                ( (TextView) findViewById(R.id.description_contents) ).setText(
                        product.getDescription());
                ( (TextView) findViewById(R.id.name_contents) ).setText(
                        product.getName());
                ( (TextView) findViewById(R.id.price_contents) ).setText(
                        Price.toString(product.getPrice()));

                long millis = product.getDateClosed().getValue() - new Date().getTime();
                long seconds = (millis / 1000) % 60;
                long minutes = (millis / (1000 * 60)) % 60;
                long hours = (millis / (1000 * 60 * 60)) % 24;
                long days = millis / (1000 * 60 * 60 * 24);
                ( (TextView) findViewById(R.id.expiry) ).setText(
                        String.format(getResources().getString(R.string.expiring_at),
                                days, hours, minutes, seconds ));

                if(product.getPictures()!=null){
                    for(String image : product.getPictures()){
                        images.add(new BitmapDrawable(BitmapCodec.StringToBitMap(image)));
                    }
                    buildImageSwitcher();
                }
            }
            else
            {
                ( (TextView) findViewById(R.id.description_contents) ).setText(R.string.not_found);
                ( (TextView) findViewById(R.id.name_contents) ).setText(R.string.not_found);
                ( (TextView) findViewById(R.id.price_contents) ).setText(R.string.not_found);
                ( (TextView) findViewById(R.id.expiry) ).setText(R.string.not_found);
            }
        }
    }

    public void buildImageSwitcher(){
        if(images.size() == 0) return;

        imageIndex = 0;
        is.setImageDrawable(images.get(imageIndex));

        is.setOnTouchListener(new SwipeDetector(is) {
            @Override
            public void onLeftToRightSwipe() {
                imageIndex = ((images.size()+imageIndex)+1)%(images.size());
                is.setImageDrawable(images.get(imageIndex));
            }
            @Override
            public void onRightToLeftSwipe() {
                imageIndex = ((images.size()+imageIndex)-1)%(images.size());
                is.setImageDrawable(images.get(imageIndex));
            }
        });
    }

    public void commitDelete(View view){
        boolean success;
        try {
            success = new deleteProductTask().execute(product_id).get();
        }
        catch (Exception e)
        {
            e.printStackTrace();
            success = false;
        }

        if( success ){
            finish();
        } else {
            //construct dialog builder
            AlertDialog.Builder error = new AlertDialog.Builder(OwnProduct.this);
            error.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {

                        }
                    }
            );
            error.setTitle("Error");
            error.setMessage("Deleting failed.");
            error.show();
        }
    }
}
