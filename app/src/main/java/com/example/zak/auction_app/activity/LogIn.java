package com.example.zak.auction_app.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.example.zak.auction_app.API.doAccount.loginUserTask;
import com.example.zak.auction_app.R;

public class LogIn extends AppCompatActivity {
    private Intent intent;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.log_in);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finishAffinity();
    }

    public void logIn(View view){
        final EditText usernameField = (EditText) findViewById(R.id.username_edit);
        final EditText passwordField = (EditText) findViewById(R.id.password_edit);

        String username = usernameField.getText().toString();
        String password = passwordField.getText().toString();

        Log.i("username", username);
        Log.i("password", password);

        // TODO: 24/09/2016 username and password field validation (valid characters, length, etc.)

        boolean success = false;
        try
        {
            success = new loginUserTask().execute(username, password).get();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        if(success){
            intent = new Intent();
            intent.putExtra("currUser", username);
            setResult(RESULT_OK, intent);
            finish();
        }
        else {
            AlertDialog.Builder error = new AlertDialog.Builder(LogIn.this);
            error.setPositiveButton("OK", new DialogInterface.OnClickListener()
                    {
                        public void onClick(DialogInterface dialog, int id) {

                        }
                    }
            );
            error.setTitle("Error");
            error.setMessage("Login failed.");
            error.show();
        }
    }

    public void register(View view){
        intent = new Intent(LogIn.this, RegisterAccount.class);
        startActivity(intent);
    }
}
