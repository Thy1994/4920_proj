package com.example.zak.auction_app.activity;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.zak.auction_app.R;
import com.example.zak.auction_backend.auctionAppApi.model.Notification;


public class NotificationArrayAdapter extends ArrayAdapter<Notification> {

    public NotificationArrayAdapter(Context context, int resource, Notification[] objects) {
        super(context, resource, objects);
    }

    @NonNull
    @Override
    public View getView(int position, View view, @NonNull ViewGroup parent ) {

        View row = view;

        if(row == null)
        {
            LayoutInflater inflater = ( (Activity) getContext() ).getLayoutInflater();
            row = inflater.inflate(R.layout.notification_view, parent, false);
        }

        final Notification notification = getItem(position);

        if(notification != null)
        {
            ( (TextView) row.findViewById(R.id.message) ).setText(notification.getMessage());
            if(!notification.getRead()) {
                ( (TextView) row.findViewById(R.id.message) ).setTypeface(null, Typeface.BOLD);
            } else {
                ( (TextView) row.findViewById(R.id.message) ).setTypeface(null, Typeface.NORMAL);
            }
        }
        else
        {
            ( (TextView) row.findViewById(R.id.message) ).setText("???");
        }


        return row;
    }
}
