package com.example.zak.auction_app.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {
    private String currUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        currUser = null;
//        currUser = "username";
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                currUser = data.getStringExtra("currUser");
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Intent intent;
        if(currUser == null) {
            intent = new Intent(MainActivity.this, LogIn.class);
            startActivityForResult(intent, 1);
        }
        else {
            intent = new Intent(MainActivity.this, ViewProducts.class);
            intent.putExtra("currUser", currUser);
            startActivity(intent);
        }
    }
}
