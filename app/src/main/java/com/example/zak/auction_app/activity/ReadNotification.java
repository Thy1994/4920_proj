package com.example.zak.auction_app.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.example.zak.auction_app.API.doAccount.readNotificationTask;
import com.example.zak.auction_app.R;

import java.util.concurrent.ExecutionException;

public class ReadNotification extends AppCompatActivity {
    long note_id = 0;
    String message;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.read_notification);

        note_id = this.getIntent().getLongExtra("note_id",0);
        message = this.getIntent().getStringExtra("message");

        boolean success = false;
        try {
             success = new readNotificationTask().execute(Long.toString(note_id)).get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        System.out.println("Reading: " + success);

        ( (TextView) findViewById(R.id.message_contents) ).setText(message);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }

    public void closeNotification(View view) {
        finish();
    }
}
